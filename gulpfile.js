'use strict'

const gulp = require('gulp')
const nodemon = require('gulp-nodemon')
const babel = require('gulp-babel')

const uglify = require('gulp-uglify')
const Cache = require('gulp-file-cache')
const mocha = require('gulp-mocha')

gulp.task('default', function () {
    return gulp.src('src/**/*.js')
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(gulp.dest('dist'))
})

gulp.task('uglifyJS', () =>
    gulp.src('./dist/**/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('dist/'))
)

const cache = new Cache()

gulp.task('compile', function () {
    const stream = gulp.src('./src/**/*.js')
        .pipe(cache.filter())
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(cache.cache())
        .pipe(gulp.dest('./dist'))

    return stream
})

gulp.task('watch', ['default'], function (done) {
    nodemon({
        script: 'dist/server.js',
        watch: 'src',
        tasks: ['compile'],
    })
})

gulp.task('test:dev', /*['default'],*/ () => {
    const mochaOptions = {
        reporter: 'nyan',
        timeout: 10000,
    }
    gulp.src('test/main-test.js', { read: false })
        .pipe(mocha(mochaOptions))
})

gulp.task('test', ['default'], () => {
    const mochaOptions = {
        reporter: 'nyan',
        timeout: 50000,
    }
    gulp.src('test/main-test.js', { read: false })
        .pipe(mocha(mochaOptions))
})
