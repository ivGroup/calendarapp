'use strict'

import logger from '../logger'
import * as sqlType from '../sqlType'
import dateLib from 'date-and-time'
import config from '../../config/config'

import * as dbController from '../db/dbController'
import * as dbLowController from '../db/dbLowController'

import * as responseCreator from './responseCreator'
import * as responseHolder from './responseHolder'




//#User
export function validateEmail(res, email) {
    if (!email || email == '') {
        responseCreator.respondWithJson(res, 400, responseHolder.emptyField('email'))
        return false
    }
    if (!sqlType.isVarchar(email, 30)) {
        responseCreator.respondWithJson(res, 400, responseHolder.notVarchar('email', 30))
        return false
    }
    return true
}

export function validatePass(res, pass) {
    if (!pass || pass == '') {
        responseCreator.respondWithJson(res, 400, responseHolder.emptyField('pass'))
        return false
    }

    if (!sqlType.isVarchar(pass, 40)) {
        responseCreator.respondWithJson(res, 400, responseHolder.notVarchar('pass', 40))
        return false
    }
    return true
}

//#Event
export function validateEventId(res, eventId) {
    if (!eventId || eventId == '') {
        responseCreator.respondWithJson(res, 400, responseHolder.emptyField('eventId'))
        return false
    }
    if (!sqlType.isInteger(eventId)) {
        responseCreator.respondWithJson(res, 400, responseHolder.notInteger('eventId'))
        return false
    }
    return true
}

export function validateEventName(res, name) {
    if (!name || name == '') {
        responseCreator.respondWithJson(res, 400, responseHolder.emptyField('name'))
        return false
    }

    if (!sqlType.isVarchar(name, 40)) {
        responseCreator.respondWithJson(res, 400, responseHolder.notVarchar('name', 40))
        return false
    }
    return true
}

export function validateEventDate(res, date) {
    if (!date || date == '') {
        responseCreator.respondWithJson(res, 400, responseHolder.emptyField('date'))
        return false
    }

    const nowInMiliseconds = (new Date())
        .getTime()
    if (!sqlType.isBigInt(date) || date < nowInMiliseconds) {
        responseCreator.respondWithJson(res, 400, responseHolder.invalidDateValue(nowInMiliseconds))
        return false
    }
    return true
}

export async function validateEventIdExist(res, eventId, userId) {
    try {
        const resultEventQuery = await dbController.event.getEvent(eventId, userId)

        if (resultEventQuery.rows.length == 0) {
            responseCreator.respondWithJson(res, 400, responseHolder.invalidEventId)
            logger.error(`Error with sql query ${e}`)
            return false
        }
        return resultEventQuery.rows[0]
    } catch (e) {
        responseCreator.respondWithJson(res, 500, responseHolder.unknownServerError)
        logger.error(`Error with sql query ${e}`)
        return false
    }
}

//#EventFilters
export function validateOneOfThree(res, ...params) {
    const logicalReducer = (a, c) => !c && a
    if (params.reduce(logicalReducer, true)) {
        responseCreator.respondWithJson(res, 400, responseHolder.oneOfThreeParamsExpected)
        return
    }
    return true
}

export function validateEventFilterDateRange(res, dateRange) {
    try {
        let [startDate, endDate] = dateRange.split('-')

        startDate = dateLib.parse(startDate, 'DD.MM.YYYY')
            .getTime()
        endDate = dateLib.parse(endDate, 'DD.MM.YYYY')
            .getTime()
        return [startDate, endDate]
    } catch (e) {
        responseCreator.respondWithJson(res, 400, responseHolder.unexpectedDateRange)
        logger.debug(e)
        return
    }
}

export function validateEventFilterTimeRange(res, timeRange) {
    try {
        let [startTime, endTime] = timeRange.split('-')

        startTime = convertTime(startTime)
        endTime = convertTime(endTime)
        return [startTime, endTime]
    } catch (e) {
        responseCreator.respondWithJson(res, 400, responseHolder.unexpectedTimeRange)
        return
    }

}

export function validateEventFilterDatFilter(res, dayFilter) {
    dayFilter.forEach(element => {
        if (!DAY_ARRAY.includes(element)) {
            responseCreator.respondWithJson(res, 400, responseHolder.unexpectedDayFilter)
            return
        }
    })
    return dayFilter
}

//#Reminder
export function validateSecondExist(res, secondsBeforeEvent) {
    if (!secondsBeforeEvent || !Number(secondsBeforeEvent)) {
        responseCreator.respondWithJson(res, 400, responseHolder.emptyField('seconds'))
        return false
    }
    return true
}

export function validateSecondProperValue(res, secondsBeforeEvent, event) {
    const nowInMillisecond = (new Date())
        .getTime()

    // console.log(event.date - secondsBeforeEvent*1000, nowInMillisecond)
    if (event.date - secondsBeforeEvent * 1000 < nowInMillisecond) {
        responseCreator.respondWithJson(res, 400, responseHolder.highSecondAmount)
        return false
    }
    return true
}

export async function validateReminderExist(res, reminderBody) {
    try {
        const reminder = await dbLowController.getReminder(reminderBody)
        if (reminder) {
            responseCreator.respondWithJson(res, 400, responseHolder.reminderTaken)
            return false
        }
        return true
    } catch (e) {
        responseCreator.respondWithJson(res, 500, responseHolder.unknownServerError)
        logger.error(`Error with lowDb query ${e}`)
        return false
    }
}

//#Extra Function
export const DAY_ARRAY = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']

export function filterByDayFilter(events, dayFilter) {
    dayFilter = convertDayFilter(dayFilter)
    return events.filter(i => {
        return isInDayFilter(i.date, dayFilter)
    })
}

export function isInTimeRange(date, timeRange) {
    const dateObject = new Date(Number(date))
    const hours = dateObject.getHours()
    const minutes = dateObject.getMinutes()
    const timeConvertedToMilliseconds = convertTime(`${hours}:${minutes}`)

    const startTime = Math.min(...timeRange)
    const endTime = Math.max(...timeRange)
    if (startTime < timeConvertedToMilliseconds && endTime > timeConvertedToMilliseconds)
        return true
    return false
}

function convertTime(time) {
    const A = dateLib.parse(`13.01.1999 ${time}:00`, 'DD.MM.YYYY HH:mm:ss')
        .getTime()
    const B = dateLib.parse(`13.01.1999 00:00:00`, 'DD.MM.YYYY HH:mm:ss')
        .getTime()
    return A - B
}


function convertDayFilter(dayFilter) {
    const newDayFilter = []
    dayFilter.forEach((dayName) => {
        newDayFilter.push(DAY_ARRAY.findIndex(element => element == dayName))
    })
    return newDayFilter
}

function isInDayFilter(date, dayFilter) {
    const dateObject = new Date(Number(date))
    const dayNum = dateObject.getDay()
    // console.log(dayNum)
    return dayFilter.includes(dayNum)
}
