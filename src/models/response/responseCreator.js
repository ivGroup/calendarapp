'use strict'

import logger from '../logger'
import * as sqlType from '../sqlType'
import * as dbController from '../db/dbController'


export function respondWithJson(res, status, message, params) {
    const stringStatus = status == 200 ? 'success' : 'error'
    const responceJson = {
        status: stringStatus,
        message: message,
        ...params,
    }
    res.status(status)
        .json(responceJson)
}
