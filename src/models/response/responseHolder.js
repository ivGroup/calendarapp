'use strict'

import logger from '../logger'
import config from '../../config/config'

export const rootRoute = 'Calendar API v1 retrieved successfully'

export const unknownServerError = `Unknown sever error !:()`

//#User
export const noUserId = `You may set userId to access protected directory`

export const invalidUserId = `Sorry, userId is invalid`

export function accessGranted(userId) {
    return `You are in user with userId[${userId}] protected directory`
}

export function successUserCreation(userId) {
    return `Success, user with id ${userId} created`
}

export function successUserLogin(userId) {
    return `Success, user with id ${userId} successfully logged in`
}

export const emailTaken = `Sorry, email is already taken`

export const invalidEmailPassword = `Email or passwprd is invalid :()`

//#Event
export const invalidEventId = `EventId is invalid`

export const successEventCreation = `Event added successfully`

export const validEventId = `Event found successfully`

export const oneOfThreeParamsExpected = `One of the parameters, dataRange, timeRange, dayFilter must be specified`

export function invalidDateValue(nowInMiliseconds) {
    return `Your [date] param is invalid, must be integer number in range from ${nowInMiliseconds} to 9223372036854775807 (in Unix format)`
}
//events filters
export const unexpectedDateRange = `Unexpected dateRange format, 04.03.2019-05.04.2019 must be...`

export const unexpectedTimeRange = `Unexpected timeRange format`

export const unexpectedDayFilter = `Unexpected dayFilter format`

//#Reminder
export const reminderTaken = `Sorry, you already have got similar reminder`

export const reminderCreated = `Reminder successfully created`

export const highSecondAmount = `Seconds amount is unexpectedly high`


//#General
export function emptyField(fieldName) {
    return `Your [${fieldName}] field can not be empty!`
}

export function notVarchar(fieldName, varcharLength) {
    return `Your [${fieldName}] param is invalid, must be ${varcharLength} character max`
}

export function notInteger(fieldName) {
    return `Your [${fieldName}] param is invalid, must be integer number in range from -2147483648 to +2147483647`
}
