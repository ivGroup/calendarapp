'use strict'

'use strict'

import logger from '../logger'
import { pool } from './dbController'
import config from '../../config/config'

export async function putEvent(...params) {
    const values = params
    return pool.query(`INSERT INTO event (userid, name, details, date) VALUES ($1, $2, $3, $4)`, values)
}

export async function getEvent(...params) {
    const values = params
    return pool.query('SELECT * FROM event WHERE id = $1 and userId = $2', values)
}

export async function getEvents(...params) {
    const values = params
    return pool.query('SELECT * FROM event WHERE date > $1 and date < $2', values)
}
