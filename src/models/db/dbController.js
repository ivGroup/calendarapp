'use strict'

import pg, { Pool } from 'pg'

import logger from '../logger'
import config from '../../config/config'

pg.defaults.poolSize = 4

import * as user from './users'
import * as event from './events'

export { user, event }

export const pool = new Pool({
    connectionString: config.connectionString
})

pool.connect()
    .then((client) => {
        logger.debug(`Successfully connected to the PostgraceSQL Database`)
        client.release()
    })
    .catch((e) => {
        logger.error(`Error in PostgraceSQL client connection`, e.stack)
    })

export async function clearUser() {
    return pool.query('DELETE FROM calendarUser', [])
}
export async function clearEvent() {
    return pool.query('DELETE FROM event', [])
}
