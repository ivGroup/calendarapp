'use strict'

import low from 'lowdb'
import FileSync from 'lowdb/adapters/FileSync'

import logger from '../logger'
import config from '../../config/config'


const adapter = new FileSync(`${config.lowName}`)
const db = low(adapter)

db.defaults({ reminder: [], count: 0 })
    .value()

export async function clearReminders() {
    const remindersAll = db.get('reminder')
        .remove()
        .write()
    console.log(remindersAll)
}


export async function getReminders() {
    const remindersAll = db.get('reminder')
        .value()
    return remindersAll
}

export async function getReminder(body) {
    const remindersAll = db.get('reminder')
        .find(body)
        .value()
    return remindersAll
}
export async function deleteRemindersById(id) {
    const result = db.get('reminder')
        .remove({ id })
        .write()
    return result
}


export async function putReminder(reminderBody) {
    reminderBody.id = Date.now()
        .toString()
    const result = db.get('reminder')
        .push(reminderBody)
        .write()
    return result
}
