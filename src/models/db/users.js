'use strict'

import logger from '../logger'
import { pool } from './dbController'
import config from '../../config/config'
import sha256 from 'sha256'

export async function getUserById(userId) {
    userId = Number(userId)
    return pool.query('SELECT * FROM calendarUser WHERE id = $1', [userId])
}

export async function getUserByEmail(email) {
    email = String(email)
    return pool.query('SELECT * FROM calendarUser WHERE email = $1', [email])
}

export async function getUserByEmailPass(...params) {
    const values = params
    values[1] = sha256(values[1])
    return pool.query('SELECT * FROM calendarUser WHERE email = $1 and pass = $2', values)
}

export async function putUser(email, pass) {
    pass = sha256(pass)
    return pool.query(`INSERT INTO calendarUser (email, pass) VALUES ($1, $2) RETURNING id, email, pass`, [email, pass])
}
