'use strict'

import SqlString from 'sqlstring'

export function isVarchar(string, num) {
    string = SqlString.escape(String(string))
    // console.log(string)
    if (string.length > (num + 2)) return false
    return true
}

export function isBigInt(num) {
    if (!Number(num)) return false
    // console.log(num)
    num = SqlString.escape(Number(num))
    if (String(num)
        .search(/[^(0-9)|-]+/) !== -1) return false
    // console.log(num)
    if (num < -92233720368547758 || num > 92233720368547758)
        return false
    return true
}

export function isInteger(num) {
    if (!Number(num)) return false
    // console.log(num)
    num = SqlString.escape(Number(num))
    // console.log(num)
    if (String(num)
        .search(/[^(0-9)|-]+/) !== -1) return false
    // console.log(num)
    if (num < -2147483648 || num > 2147483647)
        return false
    return true
}
