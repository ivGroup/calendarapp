'use strict'

import 'babel-polyfill'

import * as dbLowController from './models/db/dbLowController'
import config from './config/config'
import logger from './models/logger'

let timeoutArray = []

export default async function () {

    const allReminders = await dbLowController.getReminders()

    timeoutArray = []
    const nowInMillisecond = (new Date())
        .getTime()
    const arrayOfRemindersToDelete = []
    for (const reminder of allReminders) {
        const timeDelta = reminder.eventDate - reminder.seconds * 1000

        if (timeDelta < nowInMillisecond)
            arrayOfRemindersToDelete.push(reminder.id)
        else addReminder(reminder)
    }


    //deleting old reminders
    if (arrayOfRemindersToDelete.length > 0) {
        const promiseArray = arrayOfRemindersToDelete.map(i => {
            return dbLowController.deleteRemindersById(String(i))
        })
        Promise.all(promiseArray)
            .then(values => {
                console.log(values)
            })
    }
}

function reminderAction(userId, evenId, seconds) {
    console.log(`Я напоминаю тебе, пользователь с id ${userId}, что до ивента с id ${evenId} осталось ${seconds} секунд)`)
}

export async function addReminder(reminder) {
    const nowInMillisecond = (new Date())
        .getTime()

    const timeoutTime = (reminder.eventDate - reminder.seconds * 1000) - nowInMillisecond
    console.log(`Звонок прозивнит через ${timeoutTime/1000} sec.`)

    const timeout = setTimeout(reminderAction, timeoutTime, reminder.userId, reminder.eventId, reminder.seconds)
    timeoutArray.push(timeout)
}
