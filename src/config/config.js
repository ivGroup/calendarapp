'use strict'

const config = {}

config.port = process.env.port || 5000
config.status = process.env.NODE_ENV || 'prod'
config.connectionString = process.env.connectionString || 'postgres://zyjexqko:1dzNaxHEg77DtfeYUWKo1-VM4u7nl-A4@balarama.db.elephantsql.com:5432/zyjexqko'
config.lowName = process.env.lowName || 'db.json'

export default config
