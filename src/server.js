'use strict'

import 'babel-polyfill'

import express from 'express'
import apiRoute from './routing/apiRoute'
import bodyParser from 'body-parser'
import morgan from 'morgan'

import config from './config/config'
import logger from './models/logger'
import reminder from './reminder'


//Databse connection

const app = express()

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept'
    )
    if (req.method === 'Options') {
        res.header('Access-Control-Allow-Methods', 'PUT, GET')
        return res.status(200)
            .json({})
    } else next()
})

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

if (config.status == 'dev')
    app.use(morgan('dev'))
else app.use(morgan('tiny'))

app.use('/api/v1', apiRoute)

app.use((req, res, next) => {
    const newError = new Error('Not Found')
    newError.status = 404
    next(newError)
})

app.use((err, req, res, next) => {
    res.status(err.status || 500)
    res.json({
        error: {
            message: err.message
        }
    })
})

app.listen(config.port, () => {
    logger.debug(`Web server listening on: ${config.port}`)
})

reminder()

module.exports = app
