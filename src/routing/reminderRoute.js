'use strict'

import express from 'express'
import dateLib from 'date-and-time'

import logger from '../models/logger'
import * as sqlType from '../models/sqlType'
import * as dbLowController from '../models/db/dbLowController'
import * as dbController from '../models/db/dbController'
import { addReminder } from '../reminder'

import * as responseCreator from '../models/response/responseCreator'
import * as responseHolder from '../models/response/responseHolder'
import * as paramValidator from '../models/response/paramValidator'


const router = express.Router()


router.put('/remind', async (req, res) => {
    const userId = req.userId
    const eventId = req.query.eventId
    let secondsBeforeEvent = req.query.seconds

    if (!paramValidator.validateEventId(res, eventId)) return
    if (!paramValidator.validateSecondExist(res, secondsBeforeEvent)) return

    const event = await paramValidator.validateEventIdExist(res, eventId, userId)
    if (!event) return

    if (!paramValidator.validateSecondProperValue(res, secondsBeforeEvent, event)) return

    const reminderBody = {
        userId: userId,
        eventId: event.id,
        seconds: secondsBeforeEvent,
        eventDate: event.date
    }

    if (!await paramValidator.validateReminderExist(res, reminderBody)) return

    dbLowController.putReminder(reminderBody)
        .then(data => {
            addReminder(reminderBody)
            responseCreator.respondWithJson(res, 200, responseHolder.reminderCreated)
        })
        .catch(e => {
            responseCreator.respondWithJson(res, 500, responseHolder.unknownServerError)
            logger.error(`Error with lowDb query ${e}`)
            return
        })
})

export default router
