'use strict'

import express from 'express'

import logger from '../models/logger'
import * as sqlType from '../models/sqlType'
import * as dbController from '../models/db/dbController'

import protectedRoute from './protectedRoute'
import * as responseCreator from '../models/response/responseCreator'
import * as responseHolder from '../models/response/responseHolder'
import * as paramValidator from '../models/response/paramValidator'

const router = express.Router()

router.get('/', (req, res) => {
    responseCreator.respondWithJson(res, 200, responseHolder.rootRoute)
})

router.use('/access', protectedRoute)

router.put('/register', async (req, res, next) => {
    const email = req.query.email
    const pass = req.query.pass

    if (!paramValidator.validateEmail(res, email)) return
    if (!paramValidator.validatePass(res, pass)) return

    try {
        const result = await dbController.user.getUserByEmail(email)
        if (result.rows && result.rows.length > 0) {
            responseCreator.respondWithJson(res, 400, responseHolder.emailTaken)
            return
        }
        const insertionResult = await dbController.user.putUser(email, pass)
        const newUser = insertionResult.rows.shift()

        responseCreator.respondWithJson(res, 200, responseHolder.successUserCreation(newUser.id), { user: newUser })

    } catch (e) {
        responseCreator.respondWithJson(res, 500, responseHolder.unknownServerError)
        logger.error(`Error with sql query ${e}`)
    }
})

router.get('/login', async (req, res, next) => {
    const email = req.query.email
    const pass = req.query.pass

    if (!paramValidator.validateEmail(res, email)) return
    if (!paramValidator.validatePass(res, pass)) return

    try {
        const result = await dbController.user.getUserByEmailPass(email, pass)
        if (result.rows && result.rows.length > 0) {
            const newUser = result.rows.shift()

            responseCreator.respondWithJson(res, 200, responseHolder.successUserLogin(newUser.id), { user: newUser })
            return
        }
        responseCreator.respondWithJson(res, 400, responseHolder.invalidEmailPassword)

    } catch (e) {
        responseCreator.respondWithJson(res, 500, responseHolder.unknownServerError)
        logger.error(`Error with sql query ${e}`)
    }
})


export default router
