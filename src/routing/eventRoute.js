'use strict'

import express from 'express'
import dateLib from 'date-and-time'

import logger from '../models/logger'
import * as sqlType from '../models/sqlType'
import * as dbController from '../models/db/dbController'

import * as responseCreator from '../models/response/responseCreator'
import * as responseHolder from '../models/response/responseHolder'
import * as paramValidator from '../models/response/paramValidator'

const router = express.Router()

router.get('/event', (req, res) => {
    const eventId = req.query.eventId

    if (!paramValidator.validateEventId(res, eventId)) return

    dbController.event.getEvent(eventId, req.userId)
        .then(result => {
            if (result.rows && result.rows.length > 0) {
                const eventRow = result.rows.shift()
                responseCreator.respondWithJson(res, 200, responseHolder.validateEventId, { event: eventRow })
            } else {
                responseCreator.respondWithJson(res, 400, responseHolder.invalidEventId)
            }

        })
        .catch(e => {
            responseCreator.respondWithJson(res, 500, responseHolder.unknownServerError)
            logger.error(`Error with sql query ${e}`)
        })
})

router.put('/event', (req, res) => {
    const userId = req.userId
    const name = req.query.name
    const details = req.query.details
    const date = req.query.date

    if (!paramValidator.validateEventName(res, name)) return
    if (!paramValidator.validateEventDate(res, date)) return

    dbController.event.putEvent(userId, name, details, Number(date))
        .then(result => responseCreator.respondWithJson(res, 200, responseHolder.successEventCreation, { event: result.rows[0] }))
        .catch(e => {
            responseCreator.respondWithJson(res, 500, responseHolder.unknownServerError)
            logger.error(`Error with sql query ${e}`)
        })

})

router.get('/events', (req, res) => {
    const userId = req.userId

    let dateRange = req.query.dateRange
    let timeRange = req.query.timeRange
    let dayFilter = req.query.dayFilter
    if (dayFilter && !Array.isArray(dayFilter)) dayFilter = [dayFilter]

    let humanDate = req.query.humanDate
    let sortDate = req.query.sortDate

    if (!paramValidator.validateOneOfThree(res, dateRange, timeRange, dayFilter)) return

    if (dateRange) {
        dateRange = paramValidator.validateEventFilterDateRange(res, dateRange)
        if (!dateRange) return
    }

    if (timeRange) {
        timeRange = paramValidator.validateEventFilterTimeRange(res, timeRange)
        if (!timeRange) return
    }
    if (dayFilter) {
        dayFilter = paramValidator.validateEventFilterDatFilter(res, dayFilter)
        if (!dayFilter) return
    }


    // console.log(new Date(dateRange[0]), new Date(dateRange[1]), timeRange, dayFilter)
    dbController.event.getEvents(...dateRange)
        .then(result => {
            let events = result.rows

            //filtering by dayFilter
            if (dayFilter)
                events = paramValidator.filterByDayFilter(events, dayFilter)

            //filtering by timeRange
            if (timeRange) {
                events = events.filter(i => {
                    return paramValidator.isInTimeRange(i.date, timeRange)
                })
            }

            //converting in human date
            if (humanDate && humanDate == 'true') {
                events = events.map(i => {
                    const dateObject = new Date(Number(i.date))
                    i.humanDate = dateLib.format(dateObject, 'dddd YYYY/MM/DD HH:mm:ss')
                    return i
                })
            }

            // sorting by date
            if (sortDate && [-1, 1].includes(Number(sortDate))) {
                const sortingFunction = (i, j) => {
                    if (i.date > j.date) return Number(sortDate)
                    else if (i.date < j.date) return Number(sortDate) == 1 ? -1 : 1
                    else return 0
                }
                events.sort(sortingFunction)
            }
            responseCreator.respondWithJson(res, 200, responseHolder.validEventId, { events })

        })
        .catch(e => {
            responseCreator.respondWithJson(res, 500, responseHolder.unknownServerError)
            logger.error(`Error with sql query ${e}`)
        })
})

export default router
