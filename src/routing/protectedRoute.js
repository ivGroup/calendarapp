'use strict'

import express from 'express'

import logger from '../models/logger'
import eventRoute from './eventRoute'
import reminderRoute from './reminderRoute'
import * as dbController from '../models/db/dbController'

import * as responseCreator from '../models/response/responseCreator'
import * as responseHolder from '../models/response/responseHolder'
import * as paramValidator from '../models/response/paramValidator'

const router = express.Router()

router.param('userId', async (req, res, next, userId) => {
    dbController.user.getUserById(userId)
        .then(result => {
            if (result.rows && result.rows.length > 0) {
                const userRow = result.rows.shift()
                req.userId = userRow.id
                next()
            } else responseCreator.respondWithJson(res, 400, responseHolder.invalidUserId)
        })
        .catch(e => {
            responseCreator.respondWithJson(res, 500, responseHolder.unknownServerError)
            logger.error(`Error with userId finding ${e}`)
        })

})


router.get('/', (req, res) => {
    responseCreator.respondWithJson(res, 400, responseHolder.noUserId)
})

router.get('/:userId', (req, res) => {
    responseCreator.respondWithJson(res, 200, responseHolder.accessGranted(req.userId))
})

router.use('/:userId/data', eventRoute)
router.use('/:userId/action', reminderRoute)

export default router
