const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../../dist/server')
const expect = chai.expect

const responseHolder = require('../../dist/models/response/responseHolder')
const { email, pass } = require('../external/testData')
    .userCreds

let userId = 0
let eventId = 0
chai.use(chaiHttp)


describe('user login', () => {
    it('#1', (done) => {
        chai.request(app)
            .get('/api/v1/login')
            .query({ email, pass })
            .end((err, res) => {
                const result = JSON.parse(res.text)
                // console.log(result)
                userId = result.user.id

                expect(res)
                    .to.have.status(200)
                expect(result.message)
                    .to.equal(responseHolder.successUserLogin(userId))
                done()
            })
    })
    it('#2', (done) => {
        chai.request(app)
            .get('/api/v1/login')
            .query({ email: email + 'd', pass })
            .end((err, res) => {
                const result = JSON.parse(res.text)
                // console.log(result)

                expect(res)
                    .to.have.status(400)
                expect(result.message)
                    .to.equal(responseHolder.invalidEmailPassword)
                done()
            })
    })
    it('#3', (done) => {
        chai.request(app)
            .get('/api/v1/login')
            .query({ email: 'not empty', pass: '' })
            .end((err, res) => {
                const result = JSON.parse(res.text)

                expect(res)
                    .to.have.status(400)
                expect(result.message)
                    .to.equal(responseHolder.emptyField('pass'))
                done()
            })
    })
})

describe('user event creation', () => {
    it('#1', (done) => {
        // console.log(userId)
        chai.request(app)
            .get(`/api/v1/access/${userId}/data/event`)
            .query({ eventId: 1 })
            .end((err, res) => {
                const result = JSON.parse(res.text)

                expect(res)
                    .to.have.status(400)
                expect(result.message)
                    .to.equal(responseHolder.invalidEventId)
                done()
            })
    })
})
