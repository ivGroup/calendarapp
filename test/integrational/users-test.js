const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../../dist/server')
const sha256 = require('sha256')
const expect = chai.expect

const responseHolder = require('../../dist/models/response/responseHolder')
chai.use(chaiHttp)

const { email, pass } = require('../external/testData')
    .userCreds
let userId = 0

describe('user creation', () => {
    it('#1', (done) => {
        chai.request(app)
            .get('/api/v1/access/' + userId)
            .end((err, res) => {
                expect(res)
                    .to.have.status(400)
                const result = JSON.parse(res.text)
                expect(result.status)
                    .to.equal('error')
                expect(result.message)
                    .to.equal(responseHolder.invalidUserId)
                done()
            })
    })
    it('#2', (done) => {
        chai.request(app)
            .put('/api/v1/register')
            .query({ email, pass })
            .end((err, res) => {
                const result = JSON.parse(res.text)
                // console.log(result)
                userId = result.user.id

                expect(res)
                    .to.have.status(200)
                expect(result.user.email)
                    .to.equal(email)
                expect(result.user.pass)
                    .to.equal(sha256(pass))
                done()
            })
    })
    it('#3', (done) => {
        chai.request(app)
            .get('/api/v1/access/' + userId)
            .end((err, res) => {
                expect(res)
                    .to.have.status(200)
                const result = JSON.parse(res.text)
                expect(result.status)
                    .to.equal('success')
                expect(result.message)
                    .to.equal(responseHolder.accessGranted(userId))
                done()
            })
    })
    it('#4', (done) => {
        chai.request(app)
            .put('/api/v1/register')
            .query({ email, pass })
            .end((err, res) => {
                expect(res)
                    .to.have.status(400)
                const result = JSON.parse(res.text)

                expect(result.status)
                    .to.equal('error')
                expect(result.message)
                    .to.equal(responseHolder.emailTaken)
                done()
            })
    })
    it('#5', (done) => {
        chai.request(app)
            .put('/api/v1/register')
            .query({ email: '', pass: '' })
            .end((err, res) => {
                const result = JSON.parse(res.text)
                expect(res)
                    .to.have.status(400)
                expect(result.message)
                    .to.equal(responseHolder.emptyField('email'))
                done()
            })
    })
    it('#6', (done) => {
        chai.request(app)
            .put('/api/v1/register')
            .query({ email: 'not empty', pass: '' })
            .end((err, res) => {
                const result = JSON.parse(res.text)
                expect(res)
                    .to.have.status(400)
                expect(result.message)
                    .to.equal(responseHolder.emptyField('pass'))
                done()
            })
    })
    it('#7', (done) => {
        chai.request(app)
            .put('/api/v1/register')
            .query({ email: '12345678901234567890123456789012345678901', pass: 'not empry' })
            .end((err, res) => {
                const result = JSON.parse(res.text)
                expect(res)
                    .to.have.status(400)
                expect(result.message)
                    .to.equal(responseHolder.notVarchar('email', 30))
                done()
            })
    })
    it('#8', (done) => {
        chai.request(app)
            .put('/api/v1/register')
            .query({ email: '123456789012345678901234567890', pass: '123456789012345678901234567890123456789012345678901' })
            .end((err, res) => {
                const result = JSON.parse(res.text)
                expect(res)
                    .to.have.status(400)
                expect(result.message)
                    .to.equal(responseHolder.notVarchar('pass', 40))
                done()
            })
    })
})
