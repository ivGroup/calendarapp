const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../../dist/server')
const responseHolder = require('../../dist/models/response/responseHolder')

const expect = chai.expect

chai.use(chaiHttp)
const response404 = '{"error":{"message":"Not Found"}}'


describe('/api/v1', function () {
    it('#1', (done) => {
        chai.request(app)
            .get('/api/v1')
            .end((err, res) => {
                expect(res)
                    .to.have.status(200)
                expect(JSON.parse(res.text)
                        .message)
                    .to.equal(responseHolder.rootRoute)
                done()
            })
    })
    it('#2', function (done) {
        chai.request(app)
            .post('/404page')
            .end(function (err, res) {
                expect(res)
                    .to.have.status(404)
                expect(res.text)
                    .to.equal(response404)
                done()
            })
    })
    it('#3', function (done) {
        chai.request(app)
            .post('/api')
            .end(function (err, res) {
                expect(res)
                    .to.have.status(404)
                expect(res.text)
                    .to.equal(response404)
                done()
            })
    })
    it('#4', function (done) {
        chai.request(app)
            .post('/api/v2')
            .end(function (err, res) {
                expect(res)
                    .to.have.status(404)
                expect(res.text)
                    .to.equal(response404)
                done()
            })
    })
})
