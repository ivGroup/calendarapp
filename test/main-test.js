'use strict'

process.env.NODE_ENV = 'test'
process.env.port = 3001
process.env.connectionString = 'postgres://dojawski:x5xWdvq99AIEBaHwJ0l5v43vRUMYJIY1@isilo.db.elephantsql.com:5432/dojawski'
process.env.lowName = 'db-test.json'

const server = require('../dist/server')
const db = require('./external/db')
const dbLow = require('./external/dbLow')

db.clearDB()
dbLow.clearDB()

// require('./unit/sqlType-test')
// require('./integrational/api-test')
require('./integrational/users-test')
require('./integrational/event-test')
