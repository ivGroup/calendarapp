'use strict'

const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

const adapter = new FileSync(`${process.env.lowName}`)
const db = low(adapter)

db.defaults({ reminder: [], count: 0 })
    .value()

module.exports.clearDB = async function () {
    const remindersAll = db.get('reminder')
        .remove()
        .write()
}
