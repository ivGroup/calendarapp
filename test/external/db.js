'use strict'

const pg = require('pg')
const { Pool } = pg

pg.defaults.poolSize = 2

const pool = new Pool({
    connectionString: process.env.connectionString
})

pool.connect()
    .then((client) => {
        // console.log(`Successfully connected to the PostgraceSQL Database`)
        client.release()
    })
    .catch((e) => {
        // console.log(`Error in PostgraceSQL client connection`, e.stack)
    })

module.exports.clearDB = async function () {
    await pool.query('DELETE FROM event', [])
    await pool.query('DELETE FROM calendarUser', [])
    pool.end()
}
