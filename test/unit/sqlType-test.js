'use-strict'

var assert = require("assert")
var sqlType = require('../../dist/models/sqlType')

describe('sqlType:isVarchar', () => {
    describe('#Varchar 1', () => {
        it('#1 hould return false when the string length less than second parametr', () => {
            assert.equal(sqlType.isVarchar('123456789', 9), true)
        })
        it('#2 should return false when the string length less than second parametr', () => {
            assert.equal(sqlType.isVarchar('123456789', 10), true)
        })
        it('#3 should return false when the string length less than second parametr', () => {
            assert.equal(sqlType.isVarchar('123456789', 8), false)
        })
        it('#4 should return false when the string length less than second parametr', () => {
            assert.equal(sqlType.isVarchar(12345678999, 9), false)
        })
    })
})

describe('sqlType:isBigInt', () => {
    it('#1', () => {
        assert.equal(sqlType.isBigInt('12222'), true)
    })
    it('#2', () => {
        assert.equal(sqlType.isBigInt(111111), true)
    })
    it('#3', () => {
        assert.equal(sqlType.isBigInt('dsfghgf'), false)
    })
    it('#4', () => {
        assert.equal(sqlType.isBigInt(-3), true)
    })
    it('#5', () => {
        assert.equal(sqlType.isBigInt(-9223372036854775), true)
    })
    it('#6', () => {
        assert.equal(sqlType.isBigInt(9223372036854775), true)
    })
    it('#7', () => {
        assert.equal(sqlType.isBigInt(-92233720368547888759), false)
    })
    it('#8', () => {
        assert.equal(sqlType.isBigInt(92233720368888547759), false)
    })
})

describe('sqlType:isInteger', () => {
    it('#1', () => {
        assert.equal(sqlType.isInteger('12222'), true)
    })
    it('#2', () => {
        assert.equal(sqlType.isInteger(111111), true)
    })
    it('#3', () => {
        assert.equal(sqlType.isInteger('dsfghgf'), false)
    })
    it('#4', () => {
        assert.equal(sqlType.isInteger(-3), true)
    })
    it('#5', () => {
        assert.equal(sqlType.isInteger(-2147483648), true)
    })
    it('#6', () => {
        assert.equal(sqlType.isInteger(2147483647), true)
    })
    it('#7', () => {
        assert.equal(sqlType.isInteger(-2147483649), false)
    })
    it('#8', () => {
        assert.equal(sqlType.isInteger(2147483648), false)
    })
})
